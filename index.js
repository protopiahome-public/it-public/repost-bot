import { Telegraf } from 'telegraf';
import fs from 'fs';
import config from './config/config';

const Az = require('az');

const bot = new Telegraf(config.bot_token);

fs.readdirSync(`${__dirname}/actions`, { withFileTypes: true })
  .forEach((dirent) => {
    if (dirent.isDirectory()) {
      require(`${__dirname}/actions/${dirent.name}/index.js`).default(bot);
    }
  });

bot.catch((err, ctx) => {
  console.error(err);
});

Az.Morph.init(() => {
  bot.launch();
});

// Enable graceful stop
process.once('SIGINT', () => bot.stop('SIGINT'));
process.once('SIGTERM', () => bot.stop('SIGTERM'));
