module.exports = {
  apps: [{
    name: 'repost_bot',
    script: 'npm',
    args: 'run prod',
    watch: true,
    ignore_watch: ['*/**/data/*.*'],
  }],
};
