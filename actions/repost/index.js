import { VK } from 'vk-io';
import { Telegram } from 'telegraf';
import fs from 'fs';
import crypto from 'crypto';
import fetch from 'cross-fetch';
import repostConfig from './config';

function randU32Sync() {
  return crypto.randomBytes(4).readUInt32BE(0, true);
}

function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

const getWallName = (response, id) => {
  if (id > 0) {
    const user = response.profiles.find((profile) => profile.id === id);
    return user ? `${user.first_name} ${user.last_name}` : '';
  }
  if (id <= 0) {
    const group = response.groups.find((group) => group.id === id * -1);
    return group ? group.name : '';
  }
};

function chunkSubstr(str, size) {
  const numChunks = Math.ceil(str.length / size);
  const chunks = new Array(numChunks);

  for (let i = 0, o = 0; i < numChunks; ++i, o += size) {
    chunks[i] = str.substr(o, size);
  }

  return chunks;
}

/**
 *
 * @param {import('vk-io/lib/api/schemas/responses').WallGetResponse} response
 * @param {import('vk-io/lib/api/schemas/objects').WallWallpostFull} post
 */
const getPostText = (response, post) => {
  let text = '';
  text += `${getWallName(response, post.owner_id)}: \n\n`;
  if (post.owner_id !== post.from_id) {
    text += `${getWallName(response, post.from_id)}: \n\n`;
  }
  text += post.text.replace(/\[(.*?)\|(.*?)\]/g, '$2 ( https://vk.com/$1 )');
  text += '\n\n';
  if (post.attachments) {
    post.attachments.forEach((attachment) => {
      if (attachment.type === 'link') {
        text += `${attachment.link.title}: ${attachment.link.url}\n\n`;
      }
    });
  }
  if (post.copy_history) {
    text += post.copy_history.map((repost) => `${getPostText(response, repost)}`).join('');
  }
  return text;
};

const repost = async (bot) => {
  if (repostConfig.disabled) {
    return;
  }
  const dataPath = `${__dirname}/data/posts.json`;

  const wallCache = {};

  for (let k in repostConfig.repost_static) {
    try {
      const config = repostConfig.repost_static[k];
      config.vk_id = parseInt(config.vk_id);

      if (!fs.existsSync(dataPath)) {
        fs.writeFileSync(dataPath, '{}');
      }

      let dataKey = '';
      if (config.type === 'vk_to_telegram') {
        dataKey = `${config.type}_${config.vk_id}_${config.telegram_id}`;
      } else if (config.type === 'vk_to_vk') {
        dataKey = `${config.type}_${config.vk_id}_${config.vk_chat_id}`;
      } else {
        continue;
      }

      const data = JSON.parse(fs.readFileSync(dataPath, 'utf-8'));
      const vk = new VK({
        token: repostConfig.vk_tokens[Math.floor(Math.random() * repostConfig.vk_tokens.length)],
        language: 'ru',
      });

      console.log(dataKey);

      if (!wallCache[config.vk_id]) {
        await sleep(5000);
        wallCache[config.vk_id] = await vk.api.wall.get({
          owner_id: config.vk_id,
          count: 5,
          extended: true,
          fields: 'name',
        });
      }
      const response = wallCache[config.vk_id];
      // console.log(JSON.stringify(response, null, 2));
      let newData = false;
      if (!data[dataKey]) {
        newData = true;
        data[dataKey] = [];
      }
      const newWallData = [...data[dataKey]];
      await Promise.all(response.items.map(async (post) => {
        newWallData.push(post.id);
        if (data[dataKey].includes(post.id) || newData) {
          return;
        }
        if (post.date * 1000 < Date.now() - 1000 * 60 * 60 * 24 * 2) {
          return;
        }
        if (config.type === 'vk_to_telegram') {
          const telegram = new Telegram(config.telegram_bot_token ? config.telegram_bot_token : repostConfig.telegram_token);
          let text = getPostText(response, post);
          text += `Комментировать в ВК: https://vk.com/wall${post.owner_id}_${post.id}`;
          const textChunks = chunkSubstr(text, 4096);
          try {
            for (const k in textChunks) {
              const textChunk = textChunks[k];
              await sleep(4000);
              await telegram.sendMessage(config.telegram_id, textChunk);
            }
          } catch (e) {
            console.error(e);
          }
          return;
        }
        if (config.type === 'vk_to_vk') {
          const vkBot = new VK({ token: config.vk_bot_token });
          try {
            await vkBot.api.messages.send({
              peer_id: config.vk_chat_id,
              message: '',
              attachment: `wall${config.vk_id}_${post.id}`,
              random_id: randU32Sync(),
            });
          } catch (e) {
            console.error(e);
          }
        }
      }));
      data[dataKey] = newWallData.slice(-10);
      fs.writeFileSync(dataPath, JSON.stringify(data));
    } catch (e) {
      console.error(e);
    }
  }

  setTimeout(repost, 1000 * 60 * 15);
};

const repostActions = async (bot) => {
  repost(bot);
};

export default repostActions;
